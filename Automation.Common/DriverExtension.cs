﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.UI;

namespace Automation.Common
{
    public static class DriverExtension
    {
        private static WebDriverWait CreateWait(IWebDriver driver)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(35));
            wait.PollingInterval = TimeSpan.FromSeconds(1);
            return wait;
        }

        public static IWebElement FluentWaitByID(this IWebDriver driver, string elementID)
        {
            try
            {
                return CreateWait(driver).Until<IWebElement>(ExpectedConditions.ElementIsVisible(By.Id(elementID)));
            }
            catch (NullReferenceException ex)
            {
                Console.WriteLine(ex);
                return null;
            }
        }

        public static IWebElement FluentWaitByXpath(this IWebDriver driver, string xpath)
        {
            try
            {
                return CreateWait(driver).Until(ExpectedConditions.ElementIsVisible(By.XPath(xpath)));
            }
            catch (NullReferenceException ex)
            {
                Console.WriteLine(ex);
                return null;
            }
        }
    }
}
