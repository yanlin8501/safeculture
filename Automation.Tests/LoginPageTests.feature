﻿Feature: LoginPage
	In order to use safeculture system
	As a safeculture user
	I want to be login into safeculture system

Background: 
	Given user is able to navigate to login page "https://app.safetyculture.io"

Scenario: Valid Login
	When the following information is provided
	| Email           | Password |
	| email@email.com | password | 
	Then user should be able to login
	
Scenario Outline: Invalid Login
	When the following information is provided
	| Email   | Password   |
	| <Email> | <Password> |
	Then user should be provided validation message

	Examples: 
	| Email                | Password         |
	| validemail@email.com | invalid password |
	|                      | password         |
	| validemail@email.com |                  |


