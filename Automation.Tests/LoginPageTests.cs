﻿using Automation.Pages;
using FluentAssertions;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using System;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace Automation.Tests
{
    [Binding]
    public class LoginPageTests
    {
        private static IWebDriver driver;
        private LoginPage loginPage;
        private LoginPageData loginPageData;


        // local variables
        private string loginvalidationmessage_invalidemail = "Email is not a valid email address.";
        private string loginvalidationmessage_emptyemail = "Email must be provided";
        private string loginvalidationmessage_emptypassword = "Password must be provided";

        public LoginPageTests()
        {
            loginPage = new LoginPage(driver);
        }

        [Given(@"user is able to navigate to login page ""(.*)""")]
        public void GivenUserIsAbleToNavigateToLoginPage(string url)
        {
            driver.Navigate().Refresh();
            driver.Navigate().GoToUrl(url);
        }

        [When(@"the following information is provided")]
        public void WhenTheCorrectInformationIsProvided(Table table)
        {
            loginPageData = table.CreateInstance<LoginPageData>();

            // Input Email
            loginPage.EmailInputField.Clear();
            loginPage.EmailInputField.SendKeys(loginPageData.Email);

            // Input Password
            loginPage.PasswordInputField.Clear();
            loginPage.PasswordInputField.SendKeys(loginPageData.Password);

            // Login
            loginPage.LoginButton.Click();
        }

        [Then(@"user should be able to login")]
        public void ThenUserShouldBeAbleToLogin()
        {
            driver.Title.Should().NotBeEmpty();
            //driver.Title.Should().Be("SafetyCulture dashboard"); // Or Checking the SafetyCulture dashboard element is displayed
        }

        [Then(@"user should be provided validation message")]
        public void ThenUserShouldBeProvidedValidationMessage()
        {
            if (loginPageData.Email == "" && loginPageData.Password == "")
            {
                loginPage.LoginValidationMessageEmailEmpty.Displayed.Should().BeTrue();
                loginPage.LoginValidationMessagePasswordEmpty.Displayed.Should().BeTrue();
                loginPage.LoginValidationMessageEmailEmpty.Text.Trim().Should().Be(loginvalidationmessage_emptyemail);
                loginPage.LoginValidationMessagePasswordEmpty.Text.Trim().Should().Be(loginvalidationmessage_emptypassword);
            }

            if (loginPageData.Email == "" && loginPageData.Password != "")
            {
                loginPage.LoginValidationMessageEmailEmpty.Displayed.Should().BeTrue();
                loginPage.LoginValidationMessageEmailEmpty.Text.Trim().Should().Be(loginvalidationmessage_invalidemail);
            }

            if (loginPageData.Email != "" && loginPageData.Password != "")
            {
                loginPage.LoginValidationMessageCombinationIncorrect.Displayed.Should().BeTrue();
                loginPage.LoginValidationCombinationIncorrectResetLink.Displayed.Should().BeTrue();
            }


        }

        [BeforeFeature]
        public static void BeforeFeature()
        {
            // Initialize Driver
            if (driver == null)
            {
                driver = new FirefoxDriver();
                driver.Manage().Timeouts().SetPageLoadTimeout(TimeSpan.FromSeconds(300));
            }
            FeatureContext.Current.Set<IWebDriver>(driver, "driver");
        }

        [AfterTestRun]
        public static void AfterTestRun()
        {
            driver.Quit();
            driver.Dispose();
        }
    }
}
