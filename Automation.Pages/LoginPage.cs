using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using Automation.Common;

namespace Automation.Pages
{
    public class LoginPage
    {
        private IWebDriver _driver;

        public LoginPage(IWebDriver driver)
        {
            _driver = driver;
        }

        public IWebElement EmailInputField
        {
            get
            {
                return _driver.FluentWaitByID("email");
            }
        }

        public IWebElement PasswordInputField
        {
            get
            {
                return _driver.FluentWaitByID("password");
            }
        }

        public IWebElement LoginButton
        {
            get
            {
                return _driver.FluentWaitByXpath("//button[contains(@class,'login-button')]");
            }
        }

        public IWebElement LoginValidationMessageCombinationIncorrect
        {
            get
            {
                return _driver.FluentWaitByXpath("//div[contains(@class,'login-content')]//div[contains(@class,'alert-in')]//p[1]");
            }
        }

        public IWebElement LoginValidationMessageEmailEmpty
        {
            get
            {
                return _driver.FluentWaitByXpath("(//div[contains(@class,'login-input-container')])[1]//span[contains(@class,'input-error')]");
            }
        }

        public IWebElement LoginValidationMessagePasswordEmpty
        {
            get
            {
                return _driver.FluentWaitByXpath("(//div[contains(@class,'login-input-container')])[2]//span[contains(@class,'input-error')]");
            }
        }

        public IWebElement LoginValidationCombinationIncorrectResetLink
        {
            get
            {
                return _driver.FluentWaitByXpath("//div[contains(@class,'login-content')]//div[contains(@class,'alert-in')]//a");
            }
        }
    }
}
